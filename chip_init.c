#include <glcd.h>
#include <sim_uart1.c>
#include <global_variables.c>

char get_signal_strength();

#ifndef _CHIP_INIT_DEFINED_
    #define _CHIP_INIT_DEFINED_

    void init_system(){
        GLCDINIT_t glcd_init_data;
            
        DDRA=(1<<DDA7) | (1<<DDA6) | (0<<DDA5) | (0<<DDA4) | (0<<DDA3) | (1<<DDA2) | (0<<DDA1) | (0<<DDA0);
        PORTA=(0<<PORTA7) | (1<<PORTA6) | (1<<PORTA5) | (1<<PORTA4) | (1<<PORTA3) | (0<<PORTA2) | (0<<PORTA1) | (0<<PORTA0);


        DDRB=(0<<DDB7) | (1<<DDB6) | (1<<DDB5) | (1<<DDB4) | (1<<DDB3) | (0<<DDB2) | (0<<DDB1) | (0<<DDB0);
        PORTB=(1<<PORTB7) | (1<<PORTB6) | (1<<PORTB5) | (1<<PORTB4) | (1<<PORTB3) | (1<<PORTB2) | (0<<PORTB1) | (0<<PORTB0);

        DDRC=(1<<DDC7) | (1<<DDC6) | (0<<DDC5) | (0<<DDC4) | (0<<DDC3) | (0<<DDC2) | (0<<DDC1) | (0<<DDC0);
        PORTC=(0<<PORTC7) | (0<<PORTC6) | (0<<PORTC5) | (0<<PORTC4) | (0<<PORTC3) | (0<<PORTC2) | (0<<PORTC1) | (0<<PORTC0);

        DDRD=(1<<DDD7) | (0<<DDD6) | (0<<DDD5) | (1<<DDD4) | (0<<DDD3) | (0<<DDD2) | (0<<DDD1) | (0<<DDD0);
        PORTD=(0<<PORTD7) | (0<<PORTD6) | (0<<PORTD5) | (0<<PORTD4) | (0<<PORTD3) | (0<<PORTD2) | (0<<PORTD1) | (0<<PORTD0);

        DDRE=(0<<DDE7) | (0<<DDE6) | (0<<DDE5) | (0<<DDE4) | (0<<DDE3) | (0<<DDE2) | (0<<DDE1) | (0<<DDE0);
        PORTE=(0<<PORTE7) | (0<<PORTE6) | (0<<PORTE5) | (0<<PORTE4) | (0<<PORTE3) | (0<<PORTE2) | (0<<PORTE1) | (0<<PORTE0);

        DDRF=(0<<DDF7) | (0<<DDF6) | (0<<DDF5) | (0<<DDF4) | (0<<DDF3) | (0<<DDF2) | (0<<DDF1) | (0<<DDF0);
        PORTF=(0<<PORTF7) | (0<<PORTF6) | (0<<PORTF5) | (0<<PORTF4) | (0<<PORTF3) | (0<<PORTF2) | (0<<PORTF1) | (0<<PORTF0);

        DDRG=(0<<DDG4) | (0<<DDG3) | (0<<DDG2) | (0<<DDG1) | (0<<DDG0);
        PORTG=(0<<PORTG4) | (0<<PORTG3) | (0<<PORTG2) | (0<<PORTG1) | (0<<PORTG0);

        // Timer Period: 20 ms
        ASSR=0<<AS0;
        TCCR0=(0<<WGM00) | (0<<COM01) | (0<<COM00) | (0<<WGM01) | (1<<CS02) | (1<<CS01) | (1<<CS00);
        TCNT0=0x64;
        OCR0=0x00;

        
        TIMSK=(0<<OCIE2) | (0<<TOIE2) | (0<<TICIE1) | (0<<OCIE1A) | (0<<OCIE1B) | (0<<TOIE1) | (0<<OCIE0) | (1<<TOIE0);
        ETIMSK=(0<<TICIE3) | (0<<OCIE3A) | (0<<OCIE3B) | (0<<TOIE3) | (0<<OCIE3C) | (0<<OCIE1C);

        // USART1 2400 double
        UCSR1A=(0<<RXC1) | (0<<TXC1) | (0<<UDRE1) | (0<<FE1) | (0<<DOR1) | (0<<UPE1) | (0<<U2X1) | (0<<MPCM1);
        UCSR1B=(1<<RXCIE1) | (1<<TXCIE1) | (0<<UDRIE1) | (1<<RXEN1) | (1<<TXEN1) | (0<<UCSZ12) | (0<<RXB81) | (0<<TXB81);
        UCSR1C=(0<<UMSEL1) | (0<<UPM11) | (0<<UPM10) | (0<<USBS1) | (1<<UCSZ11) | (1<<UCSZ10) | (0<<UCPOL1);
        UBRR1H=0x00;
        UBRR1L=0x33;

        i2c_init(); //100khz
        rtc_init(3,1,0); //32768Hz

        glcd_init_data.font=font5x7;
        glcd_init_data.temp_coef=89;
        glcd_init_data.bias=3;
        glcd_init_data.vlcd=55;
        glcd_init(&glcd_init_data);

        #asm("sei")

        glcd_clear();
        milisec = 0;
        clear_buffer();

        put_command("ATZ\r\n", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
    }

    interrupt[TIM0_OVF] void timer0_ovf_isr() {
        int i = 0;
        TCNT0 = 0x64; //20 ms
        milisec += 20;

        if (milisec % 60000 == 0) { // every 60 secs
        	for (i = 1; i <= 5; i++) {
        		node[i].alarm_time_m--;
        		if (node[i].alarm_time_m == 0) {
        			switch (i) {
        				case 1:
        					relay_1 = !relay_1;
        				break;
        				case 2:
        					relay_2 = !relay_2;
        				break;
        				case 3:
        					relay_3 = !relay_3;
        				break;
        				case 4:
        					relay_4 = !relay_4;
        				break;
        				case 5:
        					relay_5 = !relay_5;
        				break;
        			}
        			node[i].status = !node[i].status;
        		}
        	}
        }

        if (is_show_clock) {

            if (milisec % 200 == 0) {
                rtc_get_time(&hour, &minute, &second);
                gui_print_time();
            }             

            if (milisec % 500 == 0) { //every 0.5 secs
                gui_colon_blink();
            }

            // if (milisec % 5000 == 0) { // every 5 secs
            //     signal_strength = get_signal_strength();
            //     glcd_setfont(phone_signal_10x5);
            //     glcd_putcharxy(PHONE_SIGNAL_X, PHONE_SIGNAL_Y, signal_strength);
            //     glcd_setfont(font5x7);
            // }
        }

        if (milisec > 4000000000) milisec = 0; //gioi han cua unsigned long
        
        //xu ly trang thai relay
        for (i = 1; i <= 5; i++) {
            if ((node[i].active_hour[hour] == true) && (minute == 0)){
        		switch (i) {
    				case 1:
    		            relay_1                 =   ON;
    		            node[1].status          =   ON;
    		            node[1].alarm_time_m    =   1;
    	        	break;
    		        case 2:
    		            relay_2                 =   ON;
    		            node[2].status          =   ON;
    		            node[2].alarm_time_m    =   1;
    		        break;
    		        case 3:
    		            relay_3                 =   ON;
    		            node[3].status          =   ON;
    		            node[3].alarm_time_m    =   1;
    		        break;
    		        case 4:
    		            relay_4                 =   ON;
    		            node[4].status          =   ON;
    		            node[4].alarm_time_m    =   1;
    		        break;
    		        }
        	}
        }
    }

    char get_signal_strength() {
        int     i               =   0,
                signal_strength =   0;

        // clear_buffer();
        put_command("AT+CSQ\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);

        for (i = 0; receive_buffer[i]; i++) {
            if (receive_buffer[i] == 'C') {
                if (receive_buffer[i+1] == 'S') {
                    if (receive_buffer[i+2] == 'Q') {
                        if (receive_buffer[i+3] == ':') {
                            if (receive_buffer[i+7] == ',') {
                                // sprintf(signal_strength,"%d",char_to_int(receive_buffer[i+5]) * 10 + char_to_int(receive_buffer[i+6]));
                                signal_strength =   char_to_int(receive_buffer[i+5]) * 10 + char_to_int(receive_buffer[i+6]);
                            }
                            if (receive_buffer[i+6] == ',') {                        
                                // sprintf(signal_strength,"%d",char_to_int(receive_buffer[i+5]));
                                signal_strength =   char_to_int(receive_buffer[i+5]);
                            }
                            // glcd_setfont(phone_signal_10x5);
                            if (signal_strength < 6) {
                                return '1';
                                // glcd_putcharxy(PHONE_SIGNAL_X, PHONE_SIGNAL_Y, '1');
                            } else if (signal_strength < 13) {
                                return '2';
                                // glcd_putcharxy(PHONE_SIGNAL_X, PHONE_SIGNAL_Y, '2');
                            } else if (signal_strength < 20) {
                                return '3';
                                // glcd_putcharxy(PHONE_SIGNAL_X, PHONE_SIGNAL_Y, '3');
                            } else if (signal_strength < 26) {
                                return '4';
                                // glcd_putcharxy(PHONE_SIGNAL_X, PHONE_SIGNAL_Y, '4');
                            } else if (signal_strength < 32) {
                                return '5';
                                // glcd_putcharxy(PHONE_SIGNAL_X, PHONE_SIGNAL_Y, '5');
                            } else {
                                return '6';
                                // glcd_putcharxy(PHONE_SIGNAL_X, PHONE_SIGNAL_Y, '0');
                            }
                            // glcd_setfont(font5x7);
                        }
                    }
                }
            }
        }
    }
#endif