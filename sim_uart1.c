#include <sim_uart1_init.c>
#include <global_variables.c>
#include <global_functions.c>

interrupt [USART1_RXC] void usart1_rx_isr(void);

int  search                             (char nguon[], char *dau);

void sms_read                           ();


void decrypt                            (unsigned char *s);
void toggle_relay                       (bool status, unsigned char *s);
void sms_send                           (unsigned char *s);
void init_sms                           ();
void init_gprs                          ();
void gprs_read                          ();
void gprs_send                          ();
void get_money_request                  ();
void call                               ();
void get_money_response_vietnammobile   ();
void get_money_response_viettel         ();
void put_money                          ();

#ifndef _SIM_FUNCTIONS_DEFINED_
    #define _SIM_FUNCTIONS_DEFINED_

    interrupt [USART1_RXC] void usart1_rx_isr(void) {
        char status,data;
        status=UCSR1A;
        data=UDR1;

        if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0) {
            rx_buffer1[rx_wr_index1++]=data;
            #if RX_BUFFER_SIZE1 == 256
            // special case for receiver buffer size=256
                if (++rx_counter1 == 0) rx_buffer_overflow1=1;
            #else
                if (rx_wr_index1 == RX_BUFFER_SIZE1) rx_wr_index1=0;
                if (++rx_counter1 == RX_BUFFER_SIZE1) {
                    rx_counter1=0;
                    rx_buffer_overflow1=1;
                }
            #endif
        }

        receive_buffer[buffer_index] = data;
        if (read_command("CMTI")) sms_read();
        if (buffer_index == BUFFER_SIZE) buffer_index = 0;
        buffer_index++;
    }

    void init_gprs() {
        put_command("AT+CIPMODE=0\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY); //bo cai nay di van chay duoc
     
        put_command("AT+CIPCSGP=1,\"internet\",\"\",\"\"\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
     
        put_command("AT+CIPHEAD=0\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
     
        put_command("AT+CIPSPRT=1\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY); 

        put_command("AT+CIPSRIP=0\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
     
        put_command("AT+CIPSCONT\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
    }

    void init_sms() {
        put_command("AT+CCALR?\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);

        put_command("ATE0\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
     
        put_command("AT+CMGF=1\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
     
        put_command("AT+CNMI=2,1,0,0,0\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
     
        put_command("AT+CSAS\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);

        put_command("AT+CMGD=1\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
    }

    void sms_read() {
        int i = 0, j = 0, k = 0;
        char command[10]; 
        int count = 0;  
        bool no_mess = true;
        memset(command, '\0', 10);
        clear_buffer();   
        is_show_clock = false;
        glcd_setfont(font5x7);
        glcd_clear(); 
        glcd_outtextxy(7,0,"Got new SMS\n");
        
        put_command("AT+CMGR=1\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
     
        put_command("AT+CMGD=1\n\r", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
     
        while (no_mess) {
            i++;                   
            if(receive_buffer[i] == '"'){
                count++;
            }
            if(count == 3){
                if(receive_buffer[i] != '"'){
                    phone_number[k] = receive_buffer[i];
                    k++;
                }    
            }            
            if ((receive_buffer[i] == '*')) {
                j = 0;
                while (receive_buffer[i-1] != '#') {
                    command[j] = receive_buffer[i];
                    i++;
                    j++;
                    if ((receive_buffer[i] == 13) && (receive_buffer[i+1] == 10)) break;
                }
                decrypt(command);
                no_mess = false;
                break;
            }
        } 
        glcd_outtextxy(7,12,"from number:");
        glcd_outtextxy(4,24,phone_number);
        glcd_outtextxy(0,36,"CMD: ");
        glcd_outtextxy(30,36,command); 
        delay_ms(5000);
        clear_buffer();
        glcd_clear();
        gui_init(); 
        delay_ms(1000);   
        is_show_clock = true;  
    }

    void sms_send(unsigned char *s) {
        is_show_clock = false;
        glcd_setfont(font5x7);
        glcd_clear(); 
        glcd_outtextxy(0,0,"Command: info");
        delay_ms(500);
        glcd_outtextxy(0,13,"Send SMS to:\n");
        put_command("AT+CMGF=1\r\n", DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRY);
        wait_until("OK", 2000);
        put_string("AT+CMGS=");
        putchar1(34);
        put_string(phone_number); 
        glcd_outtextxy(7,26,phone_number);
        putchar1(34);
        put_string("\r\n");    
        wait_until("> ", 5000); 
        while (*s) {
            putchar1(*s);
            s++;
        }
        putchar1(0x1A);
        delay_ms(1000);
        glcd_setfont(font5x7);
        glcd_outtextxy(10,39,"... done!");
        delay_ms(2000);
        glcd_clear();
        gui_init(); 
        delay_ms(1000);   
        is_show_clock = true; 
    }

    void decrypt(unsigned char *s) {
        char message[160], command[10];
        int i = 0;
        memset(command, '\0', 10);
        memset(message, '\0', 160);
         while (*s) {
            command[i] = *s;
            s++;
            i++;
        }
        
        if (strstr(command, "on")) toggle_relay(ON, command);     //find string "on" in command and excute relay
        if (strstr(command, "off")) toggle_relay(OFF, command);
        if (strstr(command, "info")) {
            strcat(message, "1: ");
            if (relay_1) {
                strcat(message, "on");
            } else {
                strcat(message, "off");
            }
             strcat(message, "\r2: ");
            if (relay_2) {
                strcat(message, " on");
            } else {
                strcat(message, " off");
            }
            strcat(message, "\r3: ");
            if (relay_3) {
                strcat(message, " on");
            } else {
                strcat(message, " off");
            }
            strcat(message, "\r4: ");
            if (relay_4) {
                strcat(message, " on");
            } else {
                strcat(message, " off");
            }
            strcat(message, "\r5: ");
            if (relay_5) {
                strcat(message, " on");
            } else {
                strcat(message, " off");
            }
             sms_send(message);
        }
    }

    void toggle_relay (bool status, unsigned char *s) {
        int DEFAULT_ALARM_DURATION = 1;
        //vd:*1on#
        char command[10], duration_in_minute[4];
        int alarm_time_m = DEFAULT_ALARM_DURATION;
        int i = 0, j = 0;

        memset(command, '\0', 10);
        memset(duration_in_minute, '\0', 10);
     
        while (*s) {
            command[i] = *s;
            s++;
            i++;
        }

        for (i = 1; command[i] != '#'; i++) {
            if ((command[i - 1] == 'n') || (command[i - 1] == 'f')) { //on hoac off
                duration_in_minute[j] = command[i];
                j++;
            }
        }

        //if (duration_in_minute[0]) alarm_time_m = atoi(duration_in_minute); //neu doc duoc cai dat gio thi doi, khong thi de mac dinh
        alarm_time_m = atoi(duration_in_minute);
        if (alarm_time_m == 0) alarm_time_m = DEFAULT_ALARM_DURATION; //neu khong co thi cho no bang mac dinh
        
        switch (command[1]) {
            case '1':
                relay_1                 =   status;
                node[1].status          =   status;
                node[1].alarm_time_m    =   alarm_time_m;
            break;

            case '2':
                relay_2                 =   status;
                node[2].status          =   status;
                node[2].alarm_time_m    =   alarm_time_m;
            break;

            case '3':
                relay_3                 =   status;
                node[3].status          =   status;
                node[3].alarm_time_m    =   alarm_time_m;
            break;

            case '4':
                relay_4                 =   status;
                node[4].status          =   status;
                node[4].alarm_time_m    =   alarm_time_m;
            break;

            default:
                relay_1                 =   status;
                relay_2                 =   status;
                relay_3                 =   status;
                relay_4                 =   status;
                for (i = 1; i < 5; i++) 
                    node[i].status      =   status;
            break;
        }
    }

    void gprs_send() {
       // PORTC.6=!PORTC.6;  
       int rcid;  
       char ahihi[];
      int i=0;   
       checker=0;
        put_string("AT+CIPSTART=\"TCP\",\"103.18.6.134\",80\n");
        wait_until("CONNECT OK", 20000);
        delay_ms(1000);
        put_string("AT+CIPSEND\r");   
        sprintf(flag,"%d",station_receive.flag);
        sprintf(temp,"%d",station_receive.temp);
        sprintf(humi,"%d",station_receive.humi);
        sprintf(sm,"%d",station_receive.sm); 
        sprintf(l,"%d",station_receive.light);
        sprintf(water,"%d",station_receive.water);
        wait_until("> ", 4000);
        put_string("GET /irrigation/php/senddata2.php?value="); 
        put_string(flag); 
        put_string(",");
        put_string(temp);
        put_string(",");
        put_string(humi);
        put_string(",");
        put_string(sm);
        put_string(",");
        put_string(l);
        put_string(",");
        put_string(water);
        put_string(" HTTP/1.1\r\n"); 
        put_string("HOST: www.openrobohus.com\r\n");
        delay_ms(100);
        put_string("Connection: close\r\n\r\n");
        putchar1(0x1A);
        wait_until("OK", 1000);   
        put_command("AT+CIPCLOSE\r\n",DEFAULT_TIMEOUT_MS,DEFAULT_MAX_RETRY); 
        strcpy(receive_buffer,strstr(receive_buffer,"rcid")); 
        for (i = 0; receive_buffer[i] != '\0'; i++) {
            // glcd_putchar(receive_buffer[i]);       // neu khong send duoc thi bo comment d?ng n�y.
             if (i % 20 == 0) {
                delay_ms(800);
                // glcd_clear();
                // glcd_outtext("done");
            }
       }  
        strcpy(ahihi,strstr(receive_buffer,"rcid"));    
        delay_ms(2000);
        if(strlen(ahihi)!=0)
        {
            rcid=search(ahihi,"rcid"); 
            if(id==rcid)
            {    
                mode=search(ahihi,"mode"); 
            }
            else
            {    
                id=rcid;
                mode=search(ahihi,"mode");  
                checker=1;       
                }   
        } 
       // glcd_outtextxy(0,10,"done"); 
         delay_ms(2000);
       // glcd_clear();
       // gui_init();    
       // is_show_clock = true;
        // clear_buffer(); 
    }

    void call(){
        // put_string("ATD+84983712941;\r\n");
        put_string("ATD+84945615998;\r\n");
    }

    void get_money_request(){
        is_get_money = true;
        is_show_clock = false;
        glcd_clear();
        glcd_setfont(font5x7);
        glcd_outtextxy(0,0,"start get money\n");    
        put_string("AT+CUSD=1,");
        putchar1('"');
        put_string("*101#");
        putchar1('"');
        put_string("\r\n"); 
        if(sim == 0)
            get_money_response_vietnammobile();
        else if(sim == 1)
            get_money_response_viettel();           
    }

    void get_money_response_vietnammobile(){
        bool is_money = true;
        unsigned char buff_money;
        int count=0, j=0;
        glcd_outtextxy(0,10,"get money response"); 
        while(is_money){
            buff_money = getchar1();
            if(buff_money == ':'){
                count++; 
            }           
            if(count == 2){
                money[j] = buff_money;
                j++;    
            }       
            if(count == 2 && buff_money == 'd')
                is_money = false;
        }        
       glcd_outtextxy(0,20,money);
       // sms_send(money);  
        delay_ms(2000);
        is_get_money = false;  
    }

    void get_money_response_viettel(){
        bool is_money = true;
        unsigned char buff_money;
        int count=0, j=0;
        glcd_outtextxy(0,10,"get money response"); 
        while(is_money){
            buff_money = getchar1();
            if(buff_money == '"'){
                count++; 
            }           
            if(count == 1 && buff_money > 45 && buff_money < 58){
                money[j] = buff_money;
                j++;    
            }       
            if(count == 1 && buff_money == 'd')
                is_money = false;
        }        
        /*glcd_clear();                    
        glcd_outtext("money: ");    
        glcd_outtext(money);*/
        sms_send(money);  
        delay_ms(2000);
        is_get_money = false;  
    }

    void put_money(){
        glcd_clear();
        glcd_outtext("put money on phone\n");
        glcd_outtext(money_code);
        put_string("AT+CUSD=1,");
        putchar1('"');
        put_string("*100*");
        put_string(money_code);
        putchar1('#');  
        putchar1('"');
        put_string("\r\n");
        delay_ms(4000);
        get_money_request(); 
    }

    int search( char nguon[], char *dau) {
        int i,a,b=0,c=0;    
        char begin[], cuoi[]="#";     
        strcpy(begin,strstr(nguon,dau));       
        a=(strlen(begin)-strlen(strstr(begin,cuoi))); 
        for (i = 5; i<a ; i++) {   
            c=begin[i]-48;  
            b=b*10+c;  
            cid[i-5]=begin[i];
          //  glcd_putchar(cid[i-5]);     
        }                
        if(strlen(begin)==0)b=-1;
        return b;  
    } 
#endif
    
