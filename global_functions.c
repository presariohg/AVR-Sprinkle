#include <global_variables.c>
#include <sim_uart1_init.c>

int  		char_to_int(char c);
char 		int_to_char(int n);
char	*int_to_str(int n);

void 		clrscr();

void 		put_string                         (unsigned char *s);
void 		put_command                        (unsigned char *s, int time_out_ms, int max_retry);
void 		clear_buffer                       ();
bool 		wait_until                         (unsigned char *s, int time_out_ms);
bool 		read_command                       (unsigned char *s);


#ifndef _GLOBAL_FUNCTIONS_DEFINED
	#define _GLOBAL_FUNCTIONS_DEFINED

	
    // void get_signal_strength();

//=================COMPUTING-RELATED FUNCTIONS=================
    int char_to_int(char c) {
        return c - '0';
    }    

    char int_to_char(int n) {
    	return n + '0';
    }

    char *int_to_str(int n) {
    	static char result[2];

    	if (n < 10) {
    		sprintf(result, "0%d",n);
    		// glcd_outtext(result);
    	} else {
    		sprintf(result, "%d", n);
    		// glcd_outtext(result);
    	}

    	return result;

    }
//=================GUI-RELATED FUNCTIONS=================
    void clrscr() {
        glcd_clear();
        glcd_moveto(0,0);
        glcd_setfont(font5x7);
    }
//=================UART-RELATED FUNCTIONS=================
    void put_string(unsigned char *s) {
        while (*s) {
            putchar1(*s);
            delay_ms(50);
            s++;
        }
    }

    void put_command(unsigned char *s, int time_out_ms, int max_retry) {
        char    command[50];
        int     i       =   0;

        memset(command,'\0',50);
        while (*s) {
            command[i] = *s;
            s++;
            i++;    
        }
        for (i = 0; i < max_retry; i++) {
            put_string(command);
            if (wait_until("OK", time_out_ms)) return; //neu khong tra ve "OK" thi lap lai
        }

        is_sim_init_ok = false;
        return;
    }

    bool wait_until(unsigned char *s, int time_out_ms) {
        char keyword[20];
        int i               =   0,
            time_start_ms   =   milisec;

        memset(keyword,'\0',20);
        while (*s) {
            keyword[i] = *s;
            s++;
            i++;
        }

        while (milisec - time_start_ms < time_out_ms) {
            if (strstr(receive_buffer, keyword)) {
                return true;
            }
        }

        return false;
    }

    bool read_command(unsigned char *s) {
	    char command[4];
	    int i               =   0;
	    memset(command,'\0',4);

	    while (*s) {
	        command[i] = *s;
	        s++;
	        i++;
	    }

	    for (i = 0; i < 4; i++) {
	        if (command[3 - i] != receive_buffer[buffer_index - i]) return false;
	    }

    	return true;
	}

	void clear_buffer() {
        memset(receive_buffer, '\0', BUFFER_SIZE);
        buffer_index = 0;
    }

    // char get_signal_strength() {

    // }

#endif