#include <global_variables.c>

#ifndef _SIM_INIT_DEFINED_
    #define _SIM_INIT_DEFINED_
    // USART1 Receiver buffer
    #define RX_BUFFER_SIZE1 64
    char rx_buffer1[RX_BUFFER_SIZE1];

    #if RX_BUFFER_SIZE1 <= 256
        unsigned char rx_wr_index1=0,rx_rd_index1=0;
    #else
        unsigned int rx_wr_index1=0,rx_rd_index1=0;
    #endif

    #if RX_BUFFER_SIZE1 < 256
        unsigned char rx_counter1=0;
    #else
        unsigned int rx_counter1=0;
    #endif

    // This flag is set on USART1 Receiver buffer overflow
    bit rx_buffer_overflow1;

    // USART1 Receiver interrupt service routine


    // Get a character from the USART1 Receiver buffer
    #pragma used+
        char getchar1(void) {
            char data;
            while (rx_counter1==0);
            data=rx_buffer1[rx_rd_index1++];
        #if RX_BUFFER_SIZE1 != 256
            if (rx_rd_index1 == RX_BUFFER_SIZE1) rx_rd_index1=0;
        #endif
        #asm("cli")
            --rx_counter1;
        #asm("sei")
            return data;
        }
    #pragma used-
    // USART1 Transmitter buffer
    #define TX_BUFFER_SIZE1 64
    char tx_buffer1[TX_BUFFER_SIZE1];

    #if TX_BUFFER_SIZE1 <= 256
    unsigned char tx_wr_index1=0,tx_rd_index1=0;
    #else
    unsigned int tx_wr_index1=0,tx_rd_index1=0;
    #endif

    #if TX_BUFFER_SIZE1 < 256
    unsigned char tx_counter1=0;
    #else
    unsigned int tx_counter1=0;
    #endif

    // USART1 Transmitter interrupt service routine
    interrupt [USART1_TXC] void usart1_tx_isr(void) {
        if (tx_counter1) {
            --tx_counter1;
            UDR1=tx_buffer1[tx_rd_index1++];
    #if TX_BUFFER_SIZE1 != 256
            if (tx_rd_index1 == TX_BUFFER_SIZE1) tx_rd_index1=0;
    #endif
        }
    }

    // Write a character to the USART1 Transmitter buffer
    #pragma used+
    void putchar1(char c) {
        while (tx_counter1 == TX_BUFFER_SIZE1);
    #asm("cli")
        if (tx_counter1 || ((UCSR1A & DATA_REGISTER_EMPTY)==0)) {
            tx_buffer1[tx_wr_index1++]=c;
    #if TX_BUFFER_SIZE1 != 256
            if (tx_wr_index1 == TX_BUFFER_SIZE1) tx_wr_index1=0;
    #endif
            ++tx_counter1;
        } else
            UDR1=c;
    #asm("sei")
    }
    #pragma used-
    
#endif